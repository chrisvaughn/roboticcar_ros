#!/usr/bin/env python
import math
import threading

import roslib
roslib.load_manifest('Unit1')

from Unit1.srv import *
from Unit1.msg import *
import rospy

# class that control the probabilities of each position
class PositionGuesser:
	def __init__(self):
		self.lock = threading.Lock()
			
		self.probs = [] # no probabilities yet
		self.pubs = [] # a publisher for each position (makes easy to plot)

		rospy.init_node('position_guesser') # creates a node

		# creates reset server
		self.s = rospy.Service('Unit1/position_guesser_reset', Reset, lambda req: PositionGuesser.handle_reset(self, req))

		# subscribe to the topics we need to get the probabilities
		rospy.Subscriber('Unit1/movement', Movement, lambda data: PositionGuesser.handle_move(self, data))
		rospy.Subscriber('Unit1/sensed_color', SensorRead, lambda data: PositionGuesser.handle_sense(self, data))
		
		# publishes the entropy of the system
		self.pub_entropy = rospy.Publisher('Unit1/entropy', Prob)

		# gets the service needed
		self.color_service = rospy.ServiceProxy('Unit1/get_position_color', PositionColor)
		
		# let's see if we're late and there is already a world
		world_size_service = rospy.ServiceProxy('Unit1/get_world_size', WorldSize)
		try:
			world_size_service.wait_for_service(0.1)
			rospy.loginfo("There is a world already. Getting it's size.")
			world_size = world_size_service().world_size
			rospy.loginfo('World size is %d.', world_size)
			self.reset_world(world_size)
		except rospy.ROSException:
			rospy.loginfo('No world yet.')

		rospy.loginfo('Ready to rock!')
		
		rospy.spin()

	# function to reset the world probabilities
	def reset_world(self, world_size):
		rospy.loginfo('Setting world size to %d.', world_size)
		self.probs = []
		self.pubs = []

		# starts with uniform probability and creates the publishers
		for i in range(world_size):
			self.probs.append(1./world_size)
			self.pubs.append(rospy.Publisher('Unit1/probability_position'+str(i), Prob))

		# published the probabilities
		self.publish_probs()

		return True

	# callback function to reset
	def handle_reset(self, req):
		with self.lock:
			return self.reset_world(req.world_size)

	# callback function to handle movement
	def handle_move(self, data):
		if (len(self.probs) == 0):
			rospy.logwarn('Need to set a world first.')
			return

		#move only 1 unit
		direction = 0
		if (data.movement < 0):
			direction = -1
		elif (data.movement > 0):
			direction = 1

		move_hit = rospy.get_param('Unit1/move_hit')

		new_probs = []
		with self.lock:
			# calculates the probabilities associated to the moviment trial
			for i in range(len(self.probs)):
				s = self.probs[i] * (1-move_hit)
				s += self.probs[(i - direction) % len(self.probs)] * move_hit
				new_probs.append(s)

			# records them
			self.probs = new_probs

			# and publishes
			self.publish_probs()

	# callback function to handle world sense
	def handle_sense(self, data):
		if (len(self.probs) == 0):
			rospy.logwarn('Need to set a world first.')
			return

		sensor_hit = rospy.get_param('Unit1/sensor_hit')
		sensed_color = data.sensed_color

		with self.lock:
			# calculates the probabilities associated to the sense trial
			for i in range(len(self.probs)):
				world_color = self.color_service(i).color
				if (sensed_color == world_color):
					self.probs[i] *= sensor_hit
				else:
					self.probs[i] *= (1-sensor_hit)

			# and publishes
			self.publish_probs()

	# function to publish the probabilities
	def publish_probs(self):
		# makes sure that the probabilities are normalized
		self.normalize()
		s = 0

		# publishes the probabilities and calculates the entropy
		for i in range(len(self.probs)):
			self.pubs[i].publish(self.probs[i])
			if (self.probs[i] > 0):
				s -= self.probs[i] * math.log(self.probs[i])

		# publishes the entropy
		self.pub_entropy.publish(s)
	
	# function to normalize the probabilities
	def normalize(self):
		s = sum(self.probs)
		for i in range(len(self.probs)):
			self.probs[i] /= s

if __name__ == '__main__':
	server = PositionGuesser()
