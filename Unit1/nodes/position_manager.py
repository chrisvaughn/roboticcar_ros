#!/usr/bin/env python
import random
import threading

import roslib
roslib.load_manifest('Unit1')

from Unit1.srv import *
from Unit1.msg import *
import rospy

# class that make the simulation
class PositionManager:
	def __init__(self):
		random.seed()

		self.lock = threading.Lock()
			
		self.world_size = 0 # no info about the world yet
		self.position = 0 # start on the most left position

		rospy.init_node('position_manager') # creates a node

		rospy.set_param('Unit1/move_hit', 1.0) # movements always succeed

		# creates service servers
		self.s1 = rospy.Service('Unit1/position_manager_reset', Reset, lambda req: PositionManager.handle_reset(self, req))
		self.s2 = rospy.Service('Unit1/position_set', PositionSet, lambda req: PositionManager.handle_set_position(self, req))

		# subscribe to the attempted moviments
		rospy.Subscriber('Unit1/movement', Movement, lambda data: PositionManager.handle_move(self, data))

		# publishes the robot position
		self.pub = rospy.Publisher('Unit1/position', Position)
		
		# let's see if we're late and there is already a world
		world_size_service = rospy.ServiceProxy('Unit1/get_world_size', WorldSize)
		try:
			world_size_service.wait_for_service(0.1)
			rospy.loginfo("There is a world already. Getting it's size.")
			self.world_size = world_size_service().world_size
			rospy.loginfo('World size is %d.', self.world_size)
		except rospy.ROSException:
			rospy.loginfo('No world yet.')

		rospy.loginfo('Ready to rock!')

		rospy.spin()

	# callback function to reset
	def handle_reset(self, req):
		rospy.loginfo('Setting world size to %d.', req.world_size)
		with self.lock:
			self.world_size = req.world_size
			self.positon = 0
			self.pub.publish(self.position)
			return True

	# callback to force position to a place
	def handle_set_position(self, req):
		position = req.position
		rospy.loginfo('Setting position to %d.', position)
		with self.lock:
			# checks if position is valid
			if (position < 0 or position >= self.world_size) :
				rospy.logwarn('Position requested is out of bound [0,%d].', self.world_size-1)
				return False

			# sets internal position and publishes
			self.position = position
			self.pub.publish(self.position)
			return True

	# callback to handle a moviment request
	def handle_move(self, data):
		rospy.loginfo('Trying to move.')
		if (self.world_size == 0):
			rospy.logwarn('Need to set a world first.')
			return

		#move only 1 unit
		direction = 0
		if (data.movement < 0):
			direction = -1
		elif (data.movement > 0):
			direction = 1

		rospy.loginfo('Direction is %d.', direction)

		with self.lock:
			# checks if the robot will move this time
			if (random.uniform(0,1) <= rospy.get_param('Unit1/move_hit')):
				rospy.loginfo('Moved successfully!')
				self.position = (self.position + direction) % self.world_size

			self.pub.publish(self.position)

if __name__ == '__main__':
	server = PositionManager()
