#!/usr/bin/env python
import random
import threading

import roslib
roslib.load_manifest('Unit1')

from Unit1.srv import *
from Unit1.msg import *
import rospy

# class that senses the color of a position of the world
class Sensor:
	def __init__(self):
		random.seed()

		self.lock = threading.Lock() # create a lock
			
		self.world_size = 0 # no info about the world yet

		rospy.init_node('sensor') # creates a node

		rospy.set_param('Unit1/sensor_hit', 1.0) # sersor always gets the right color

		# creates the reset server
		self.s = rospy.Service('Unit1/sensor_reset', Reset, lambda req: Sensor.handle_reset(self, req))

		# wants to be informed the position changes
		# everytime a new position is published, the sensor publishes the sensed color
		rospy.Subscriber('Unit1/position', Position, lambda data: Sensor.handle_position(self, data))

		# publisher for the real and sensed color
		self.pub_real = rospy.Publisher('Unit1/real_color', SensorRead)
		self.pub_sensed = rospy.Publisher('Unit1/sensed_color', SensorRead)

		# creates a proxy for the colors services
		self.color_service = rospy.ServiceProxy('Unit1/get_position_color', PositionColor)
		self.other_color_service = rospy.ServiceProxy('Unit1/get_other_color', DifferentColor)

		# let's see if we're late and there is already a world
		world_size_service = rospy.ServiceProxy('Unit1/get_world_size', WorldSize)
		try:
			world_size_service.wait_for_service(0.1)
			rospy.loginfo("There is a world already. Getting it's size.")
			self.world_size = world_size_service().world_size
			rospy.loginfo('World size is %d.', self.world_size)
		except rospy.ROSException:
			rospy.loginfo('No world yet.')

		rospy.loginfo('Ready to rock!')

		rospy.spin()

	# callback function to reset
	def handle_reset(self, req):
		rospy.loginfo('Setting world size to %d.', req.world_size)
		with self.lock:
			self.world_size = req.world_size
			return True

	# callback function to sense the color of a new position
	def handle_position(self, data):
		position = data.position # new position
		rospy.loginfo('Got new position %d.', position)

		# checks if can sense world
		try:
			self.color_service.wait_for_service(0.1)
		except rospy.ROSException:
			rospy.logwarn('But got no world to sense.')
			return

		with self.lock:
			# checks if position is valid
			if (position < 0 or position >= self.world_size):
				rospy.logwarn('Position requested is out of bound [0,%d].', self.world_size-1)
				return

			# gets the color for the position
			color = self.color_service(position).color
			rospy.loginfo('Got real color "%s".', color)

			# publish the real color
			self.pub_real.publish(color)

			# checks is sensor will be right this time
			if (random.uniform(0,1) <= rospy.get_param('Unit1/sensor_hit')):
				rospy.loginfo('Sensed the correct color.')
				self.pub_sensed.publish(color)
			else:
				other_color = self.other_color_service(color).color
				rospy.loginfo('Sensed wrong color "%s".', other_color)
				self.pub_sensed.publish(other_color)

if __name__ == '__main__':
	server = Sensor()
