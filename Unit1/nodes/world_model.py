#!/usr/bin/env python
import random
import threading

import roslib
roslib.load_manifest('Unit1')

from Unit1.srv import *
import rospy

# class that defines how the world is
class WorldModel:
	def __init__(self):
		random.seed()

		self.lock = threading.Lock() # create a lock

		self.world = [] # world is empty at the beginning

		rospy.init_node('world_model') # creates a node

		# creates all services provided
		self.s1 = rospy.Service('Unit1/modify_world', ModifyWorld, lambda req: WorldModel.handle_modify_world(self, req))
		self.s2 = rospy.Service('Unit1/get_world_size', WorldSize, lambda req: WorldModel.handle_get_world_size(self, req))
		self.s3 = rospy.Service('Unit1/get_position_color', PositionColor, lambda req: WorldModel.handle_get_position_color(self, req))
		self.s4 = rospy.Service('Unit1/get_other_color', DifferentColor, lambda req: WorldModel.handle_get_other_color(self, req))

		# creates a proxy for each service we need
		self.sensor_reset_service = rospy.ServiceProxy('Unit1/sensor_reset', Reset)
		self.position_manager_reset_service = rospy.ServiceProxy('Unit1/position_manager_reset', Reset)
		self.position_guesser_reset_service = rospy.ServiceProxy('Unit1/position_guesser_reset', Reset)

		# everything created. Let's rock!
		rospy.loginfo('Ready to rock!')

		# run rospy forever
		rospy.spin()

	# callback function to change the world
	def handle_modify_world(self, req):
		rospy.loginfo('World change requested.')
		new_world = req.world.split() # gets the string provided

		rospy.loginfo('New world has size %d and is %s.', len(new_world), new_world)
		
		# if the world is too small, the robot doesn't have a doubt where it is (or there is no position for it!)
		if (len(new_world) < 2):
			rospy.logwarn('World should have size at least 2.')
			return 0

		# if the world is too big, we can't plot the probabilities. you can remove this if you don't want the plot
		if (len(new_world) > 7):
			rospy.logwarn('World should have size at most 7.')
			return 0

		# makes sure no other method is using world or resetting remotes
		with self.lock:
			self.world = new_world

			# the new world is good. let's propagate the news that we have a new world!
			# every reset server gets the size of the new world and returns if the reconfiguration worked
			rospy.loginfo('Configuring sensor to new world...')
			try:
				# checks if there is someone offering the service
				self.sensor_reset_service.wait_for_service(0.1)
				ans = self.sensor_reset_service(len(self.world)).reseted
				if (ans):
					rospy.loginfo('Sensor configured correctly!')
				else:
					rospy.logerr('Error configuring sensor!')
			except rospy.ROSException:
				rospy.logwarn('No sensor available.')

			rospy.loginfo('Configuring position manager to new world...')
			try:
				self.position_manager_reset_service.wait_for_service(0.1)
				ans = self.position_manager_reset_service(len(self.world)).reseted
				if (ans):
					rospy.loginfo('Position manager configured correctly!')
				else:
					rospy.logerr('Error configuring position manager! Exiting...')
			except rospy.ROSException:
				rospy.logwarn('No position manager available.')

			rospy.loginfo('Configuring position guesser to new world...')
			try:
				self.position_guesser_reset_service.wait_for_service(0.1)
				ans = self.position_guesser_reset_service(len(self.world)).reseted
				if (ans):
					rospy.loginfo('Position guesser configured correctly!')
				else:
					rospy.logerr('Error configuring position guesser! Exiting...')
			except rospy.ROSException:
				rospy.logwarn('No position guesser available.')

			# informs the requester the size of the world got
			return len(self.world)

	# callback function to get the world size
	def handle_get_world_size(self, req):
		rospy.loginfo('Someone is late and requesting world size.')
		with self.lock: # makes sure we aren't getting
			return len(self.world)

	# callback function to get the color of a position
	def handle_get_position_color(self, req):
		position = req.position
		rospy.loginfo('Getting color for position %d...', position)
		
		with self.lock:
			if (position >= len(self.world) or position < 0): # check if requester is insane
				rospy.logwarn('Requested position is out of bound [0,%d].', len(self.world)-1)
				return ''

			rospy.loginfo("Color is %s.", self.world[position])
			return self.world[position]

	def handle_get_other_color(self, req):
		color = req.color
		rospy.loginfo('Requiring a different color than %s.', color)
		with self.lock:
			world_colors = list(set(self.world))

			if (world_colors.count(color) == 0):
				rospy.logwarn('Color not found in world. Return the same color.')
				return color

			world_colors.remove(color)
			index = random.randint(0, len(world_colors)-1)
			new_color = world_colors[index]
			rospy.loginfo('Got color %s.', new_color)
			return new_color

if __name__ == '__main__':
	server = WorldModel()
