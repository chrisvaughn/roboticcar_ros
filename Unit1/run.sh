#!/bin/sh
map="red green red green green"

rosservice call /Unit1/modify_world "$map"

rosparam set /Unit1/move_hit 0.8
rosparam set /Unit1/sensor_hit 0.8

n=`rostopic list | grep probability_position | wc -l`

names="/Unit1/probability_position0/prob"
legend="pos_0"

for i in `seq 1 $((n-1))`
do
  names=$names" /Unit1/probability_position$i/prob"
  legend=$legend",pos_$i"
done

rxplot -p 15 $names /Unit1/entropy/prob --legend="$legend,entropy" &

rostopic pub /Unit1/movement Unit1/Movement 1 -r 5
